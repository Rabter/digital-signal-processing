function lab_9()
I=double(imread('im01.bmp')) / 255;

figure;
imshow(I); 
title('Source image');

PSF=fspecial('motion', 54, 65);
[J1, ~]=deconvblind(I, PSF);
figure;
imshow(J1);
title('Recovered image');
end