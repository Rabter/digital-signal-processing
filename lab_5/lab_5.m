function lab_5

% Input parameters
sigma = 0.5;

% Calculation range
mult = 5;
step = 0.005;
t = -mult:step:mult;

% Signal generation
x0 = exp(-(t/sigma).^2);

% Gaussian noise generation
NA = 0;
NS = 0.05;
n1 = normrnd(NA,NS,[1 length(x0)]);
x1 = x0+n1;

% Impulsive noise generation
count = 7;
M = 0.4;
n2 = impnoise(length(x0),count,M);
x2 = x0+n2;

% Calculation of filtering arrays
G = gaussfilt(4,20);
BB = buttfilt(6,20);

figure(1)
plot(t,x0,t,x1,t,x2);
title('Original signals');
legend('Without noise','Gaussian noise','Impulsive noise');

figure(2)
plot(t,x0,t,x1-filtfilt(G,1,x1),t,x2-filtfilt(G,1,x2));
title('Gaussian filtering');
legend('Without noise','Gaussian noise','Impulsive noise');

figure(3)
plot(t,x0,t,x1-filtfilt(BB,1,x1),t,x2-filtfilt(BB,1,x2));
title('Non-recursive Butterworth filtering');
legend('Without noise','Gaussian noise','Impulsive noise');

end

% Impulsive noise generation
function y = impnoise(size,N,mult)
    step = floor(size/N);
    y = zeros(1,size);
    for i = 0:floor(N/2)
        y(round(size/2)+i*step) = mult*(0.5+rand);
        y(round(size/2)-i*step) = mult*(0.5+rand);
    end
end

% Non-recursive Butterworth implementation
function y = buttfilt(D,size)
    x = linspace(-size/2,size/2,size);
    y = 1./(1+(D./x).^4);
    y = y/sum(y);
end

% Non-recursive Gaussian implementation
function y = gaussfilt(sigma,size)
    x = linspace(-size/2,size/2,size);
    y = 1 - exp(-x.^2/(2*sigma^2));
    y = y/sum(y);
end