function lab_2()

T = 2.0;
sigma = 0.5;

% Calculation range
tmax = 5;
t = -tmax:0.05:tmax;

% Calculation of pulse functions
rect_original = zeros(size(t));
rect_original(abs(t) - T < 0) = 1;
rect_original(abs(t) == T) = 0.5;
gauss_original = exp(-(t/sigma).^2);

% FFT of functions (with "twin" effect)
rect_fft_twin = fft(rect_original);
gauss_fft_twin = fft(gauss_original);

% FFT of functions (without "twin" effect)
rect_fft = fftshift(rect_fft_twin);
gauss_fft = fftshift(gauss_fft_twin);

% DFT of functions (with "twin" effect)
rect_dft_twin = dft(rect_original);
gauss_dft_twin = dft(gauss_original);

% DFT of functions (without "twin" effect)
rect_dft = fftshift(rect_dft_twin);
gauss_dft = fftshift(gauss_dft_twin);


xs = 0:length(t)-1;

figure (1);
subplot(2,1,1);
plot(xs, abs(rect_fft_twin)/length(xs),xs,abs(rect_fft)/length(xs));
title('rect: FFT');
legend('With "twin" effect','Without "twin" effect');
subplot(2,1,2);
plot(xs,abs(rect_dft_twin)/length(xs),xs,abs(rect_dft)/length(xs));
title('rect: DFT');
legend('With "twin" effect','Without "twin" effect');

figure (2);
subplot(2,1,1);
plot(xs,abs(gauss_fft_twin)/length(xs),xs,abs(gauss_fft)/length(xs));
title('Gauss: FFT');
legend('With "twin" effect','Without "twin" effect');

subplot(2,1,2);
plot(xs,abs(gauss_dft_twin)/length(xs),xs,abs(gauss_dft)/length(xs));
title('Gauss: DFT');
legend('With "twin" effect','Without "twin" effect');

end

% Discrete Fourier Transform function
function y = dft(x)
a = 0:length(x)-1;
b = -2 * pi * sqrt(-1) * a / length(x);
for i = 1:length(a)
a(i) = 0;
for j = 1:length(x)
a(i) = a(i) + x(j) * exp(b(i) * j);
end
end
y = a;
end