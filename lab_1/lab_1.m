function lab_1()

    sigma = 2;
    L = 3;
    A = 1;

    tmax = input('Input signal range: ');
    dt = input('Input step: ');
    n = tmax / dt;
    tmax = tmax / 2;
    t = -tmax:dt:tmax; 

    gauss_discrete = A * exp(-(t / sigma).^2);
    rect_discrete = zeros(size(t));
    rect_discrete(abs(t) - L < 0) = 1;

    x = -tmax:0.005:tmax;
    gauss_original = A * exp(-(x / sigma).^2);
    rect_original = zeros(size(x));
    rect_original(abs(x) - L < 0) = 1;

    gauss_restored = zeros(1, length(x));
    rect_restored = zeros(1, length(x));
    for i=1:length(x)
        for j = 1:n
            gauss_restored(i) = gauss_restored(i) + gauss_discrete(j) * mysinc(pi * (x(i) - t(j))/dt);
            rect_restored(i) = rect_restored(i) + rect_discrete(j) * mysinc(pi * (x(i) - t(j))/dt);
        end
    end

    figure;
    subplot(2,1,1);
    title('Gaussian filter');
    hold on;
    grid on;
    plot(x, gauss_original, 'b');
    plot(x, gauss_restored, 'k');
    plot(t, gauss_discrete, '.m');
    legend('Original', 'Restored', 'Discrete');

    subplot(2,1,2);
    title('Rectangular function');
    hold on;
    grid on;
    plot(x, rect_original, 'b');
    plot(x, rect_restored, 'k');
    plot(t, rect_discrete, '.m');
    legend('Original', 'Restored', 'Discrete');

end

function sinc = mysinc(x)
    if x == 0
        sinc = 1;
    else
        sinc = sin(x)./x;
    end
end