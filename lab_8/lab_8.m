F = 3; 
dt = 0.05;
x = -F:dt:F;
N = length(x); 
eps = 0.05;

noizy = create_signal(x);

signal = noizy;
for i = 1 :  N
    smthm = mean(noizy, i); 
    if (abs(signal(i) - smthm) > eps)
        signal(i) = smthm; 
    end
end

figure 
hold on; 
plot(x, noizy);
plot(x, signal); 
title('Mean smoothed');
legend('Original','Smoothed');
hold off; 

noizy = create_signal(x);

signal = noizy;
for i = 1 : N
    smthm = med(noizy, i); 
    if (abs(signal(i) - smthm) > eps)
        signal(i) = smthm; 
    end
end

figure 
hold on; 
plot(x, noizy);
plot(x, signal); 
title('Median smoothed');
legend('Original','Smoothed');
hold off;

function y = mean(ux, i)
r = 0;
imin = i - 2; 
imax = i + 2; 
for j = imin : 1 : imax
    if (j > 0 && j < (length(ux) + 1))
        r = r + ux(j); 
    end
end
r = r / 5; 
y = r; 
end

function y = med(ux, i)
imin = i - 1;
imax = i + 1;
if (imin < 1)
    ir = ux(imax); 
else
    if (imax > length(ux))
        ir = ux(imin); 
    else
        if (ux(imax) > ux(imin))
            ir = ux(imin); 
        else
            ir = ux(imax); 
        end
    end
end
y = ir; 
end

function signal = create_signal(x)
    a = 1;
    sigma = 1;
    signal = a * exp(-x.^2 / sigma ^ 2);
    imp = 0.25;
    
    signal = signal + impnoise(length(x), 7, imp);
end

function y = impnoise(size,N,mult)
    step = floor(size/N);
    y = zeros(1,size);
    for i = 1:N
        ir = round(i * step);
        y(ir) = mult * rand;
    end
end

